<?

class Dictionary
{
    private $dictFile;
    
    function __destruct()
    {
        fclose( $this->dictFile );
    }
    
    // parameters - from & to
    function Initialize( $args )
    {
        $dictFilename = "";
        if ( $args["language"] == "ido_to_english" )
        {
            $dictFilename = "ido-to-en.txt";
        }
        else if ( $args["language"] == "english_to_ido" )
        {
            $dictFilename = "en-to-ido.txt";
        }
        else if ( $args["language"] == "espanol_to_ido" )
        {
            $dictFilename = "es-to-ido.txt";
        }
        else if ( $args["language"] == "ido_to_espanol" )
        {
            $dictFilename = "ido-to-es.txt";
        }
        else if ( $args["language"] == "ido_to_nihongo" )
        {
            $dictFilename = "ido-to-jp.txt";
        }
        else if ( $args["language"] == "deutsch_to_ido" )
        {
            $dictFilename = "de-to-ido.txt";
        }
        else if ( $args["language"] == "ido_to_deutsch" )
        {
            $dictFilename = "ido-to-de.txt";
        }
        else if ( $args["language"] == "nihongo_to_ido" )
        {
            $dictFilename = "jp-to-ido.txt";
        }
        else if ( $args["language"] == "ido_to_italiano" )
        {
            $dictFilename = "ido-to-it.txt";
        }
        else if ( $args["language"] == "ido_to_russian" )
        {
            $dictFilename = "ido-to-ru.txt";
        }
        else if ( $args["language"] == "ido_to_portugues" )
        {
            $dictFilename = "ido-to-pt.txt";
        }
        else if ( $args["language"] == "ido_to_suomi" )
        {
            $dictFilename = "ido-to-fi.txt";
        }
        else if ( $args["language"] == "ido_to_francais" )
        {
            $dictFilename = "ido-to-fr.txt";
        }
        else if ( $args["language"] == "ido_to_nederlands" )
        {
            $dictFilename = "ido-to-nl.txt";
        }
        else if ( $args["language"] == "ido_to_esperanto" )
        {
            $dictFilename = "ido-to-eo.txt";
        }
	else if ( $args["language"] == "esperanto_to_ido" )
	{
		$dictFilename = "eo-to-ido.txt";
	}
	else if ( $args["language"] == "ido_to_interlingua" )
	{
		$dictFilename = "ido-to-interlingua.txt";
	}
	else if ( $args["language"] == "en_to_ido_phrases" )
	{
		$dictFilename = "en-to-ido-phrases.txt";
	}
        
        if ( $dictFilename != "" )
        {
            $this->dictFile = fopen( "content/dictionary/" . $dictFilename, "r" );
        }
        else
        {
        }
    }
    
    function SearchForWord_Starting( $word )
    {
        $results = array();
        // Search entire text file
        while ( $line = strtolower( fgets( $this->dictFile ) ) )
        {
            $pos = strpos( $line, $word );
            
            // Mark as found
            if ( $pos !== false && $pos == 0 &&
                    $results != "" && $line != "" )
            {
                array_push( $results, $line );
            }
        }
        
        // Move file pointer back to start
        rewind( $this->dictFile );
        
        return $results;
    }
    
    
    function SearchForWord_Liberal( $word )
    {
        $results = array();
        // Search entire text file
        while ( $line = strtolower( fgets( $this->dictFile ) ) )
        {
            $pos = strpos( $line, $word );
            
            // Mark as found
            if ( $pos !== false &&
                    $results != "" && $line != ""  )
            {
                array_push( $results, $line );
            }
        }
        
        // Move file pointer back to start
        rewind( $this->dictFile );
        
        return $results;
    }
    
    
    function SearchForWord_Regex( $word )
    {
        $results = array();
        // Search entire text file
        while ( $line = strtolower( fgets( $this->dictFile ) ) )
        {
            $regexMatch = preg_match( $word, $line );
            
            // Mark as found
            if ( $regexMatch == 1 )
            {
                array_push( $results, $line );
            }
        }
        
        // Move file pointer back to start
        rewind( $this->dictFile );
        
        return $results;
    }
    
    
    function SearchForWord_Wildcard( $word )
    {
    }
    
    
    // parameters - type, search
    function FindMatches( $args )
    {
        $allWords = explode( " ", $args["search"] );
        $allResults = array();
        
        foreach( $allWords as $word )
        {
			$allResults[ $word ] = Array();
			
            if ( $args["type"] == "starting" )
            {
                $wordResults = $this->SearchForWord_Starting( strtolower( $word ) );
                //array_push( $allResults, $wordResults );
                $allResults[ $word ] = $wordResults;
            }
            else if ( $args["type"] == "liberal" )
            {
                $wordResults = $this->SearchForWord_Liberal( strtolower( $word ) );
                //array_push( $allResults, $wordResults );
                $allResults[ $word ] = $wordResults;
            }
            else if ( $args["type"] == "regex" )
            {
                $wordResults = $this->SearchForWord_Regex( strtolower( $word ) );
                //array_push( $allResults, $wordResults );
                $allResults[ $word ] = $wordResults;
            }
            else if ( $args["type"] == "wildcard" )
            {
            }
        }
        
        return $allResults;
    }
}

?>
